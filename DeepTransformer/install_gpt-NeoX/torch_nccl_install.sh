

export USE_OPENCV=1 \
export BUILD_TORCH=ON \
export CMAKE_PREFIX_PATH="/usr/bin/" \
export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/lib:$LD_LIBRARY_PATH \
export CUDA_BIN_PATH=/usr/local/cuda/bin \
export CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda/ \
export CUDNN_LIB_DIR=/usr/local/cuda/lib64 \
export CUDA_HOST_COMPILER=cc \
export USE_CUDA=1 \
export USE_NNPACK=1 \
export CC=cc \
export CXX=c++ \
export TORCH_CUDA_ARCH_LIST="3.5 5.2 6.0 6.1+PTX" \
export TORCH_NVCC_FLAGS="-Xfatbin -compress-all" \
export USE_SYSTEM_NCCL=1 \

